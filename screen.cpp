#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <stdio.h>

const int SCREEN_HEIGHT = 500;
const int SCREEN_WIDTH = 500;

int screenmain()
{
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    //glEnable(GL_TEXTURE_2D);//
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT),
     "The code",
       sf::Style::Default,
        settings);
    /* Window settings */
    window.setVerticalSyncEnabled(true);
    
    /*
    sf::CircleShape shape(100.f, 3);
    shape.setFillColor(sf::Color::Green);
*/
    sf::ConvexShape shape;
    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(0, 0));
    shape.setPoint(1, sf::Vector2f(100, 0));
    shape.setPoint(2, sf::Vector2f(100, 200));
    bool running = true;
    while(running)
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch(event.type){
                case sf::Event::Closed:
                    running = false;
                    break;
                case sf::Event::KeyPressed:
                    printf("woh\n");
                    break;
                case sf::Event::Resized:
                    glViewport(0, 0, event.size.width, event.size.height);
                    break;
            }
        }
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        window.clear();
        window.draw(shape);
        window.display();
    }
    window.close();
    return 0;
}
