#ifndef HEADER_STATS
#define HEADER_STATS

#include <vector>

template <typename T>
class Statistics
{
    private:
    public:
        T mean(std::vector<T> set);
};
#endif
