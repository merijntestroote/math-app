#include <iostream>
#include <string>
#include <vector>

#include "statistics.h"
#include "tools.cpp"

template <typename T>
T Statistics<T>::mean(std::vector<T> set)
{
    using namespace std;
    T total = 0;
    int numberOfInputs = 0;
    typename vector<T>::iterator it;
    for(it = set.begin(); it != set.end(); ++it){
        total += *it;
        numberOfInputs++; 
    } 
    return total / numberOfInputs;
}
