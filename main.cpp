#include <iostream>
#include <vector>

#include "statistics.cpp"
#include "screen.cpp"

int main()
{
    using namespace std;
    double input = 0.0; 
    vector<double> inputs;
    while(1){
        try {
            input = getValidatedInput<double>();
        } catch(exception e) {
            break;
        }
        inputs.push_back(input);
    }
    Statistics<double> stats;
    cout << "Mean: " << stats.mean(inputs) << endl;
}
