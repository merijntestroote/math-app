#include <iostream>
#include <ios>

template <typename T>
T getValidatedInput()
{
    using namespace std;
    T result;
    cin >> result;

    if(cin.fail() || cin.get() != '\n'){
        cin.clear();
        while(cin.get() != '\n')
            ;
        throw ios_base::failure("Invalid input.");
    }
    return result;
}


